# report_generator使用说明

## 功能介绍

用于dtse工作中的issuereporter平台的每周报告生成，自动统计应用数、issuereporter数、本周处理issuereporter单数量及闭环率等

## 使用说明

1. 依赖库：`pandas`
2. issuerporter导出的Excel文件，并将文件名命名为对应应用的名称，置于export_files目录下
3. `python report_generator.py`